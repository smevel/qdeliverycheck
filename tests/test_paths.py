from pathlib import Path

_HERE = Path(__file__).resolve().parent

ROOT = _HERE.parent

LONG_PATH_LIMIT = 130

def test_path_length():
    """ Checks we don't have long paths that can break on windows (260 char limit).

    We check a shorter length because the root QGIS path can be long too.
    """
    plugin_path = ROOT / 'QDeliveryCheck'
    for test_path in plugin_path.glob('**/*'):
        relative_path = test_path.relative_to(ROOT)
        assert len(str(relative_path)) < LONG_PATH_LIMIT

