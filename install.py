import os
from pathlib import Path
import shutil

import pyqt5ac
import click


_HERE = Path(__file__).resolve().parent

_PLUGIN_NAME = 'QDeliveryCheck'


@click.command()
@click.option('-e', '--editable', is_flag=True, help="Create a symbolic link")
@click.option('-p', '--profile', default='default', help="QGIS profile name (default to 'default')")
@click.option('-r', '--root', default=None, help="QGIS root (default to '~/.local/share/QGIS/QGIS3/')")
@click.option('-f', '--force', is_flag=True, help="Don't ask before deleting existing install")
def install(editable, profile, root, force):
    """ Install the plugin by copying it to a local QGIS installation.

    For development, option --editable will create a symbolic link instead
    of copy.
    """
    if root is None:
        root = Path.home() / '.local' / 'share' / 'QGIS' / 'QGIS3'

    plugins_dir = Path(root) / 'profiles' / profile / 'python' / 'plugins'
    plugin_path = plugins_dir / _PLUGIN_NAME

    if plugin_path.exists():
        # remove existing install
        should_delete = (
            force or
            click.confirm(f"Existing install '{plugin_path}' detected. Do you want to delete it ?")
        )
        if should_delete:
            if plugin_path.is_symlink():
                plugin_path.unlink()
            elif plugin_path.is_dir():
                shutil.rmtree(plugin_path)
            click.echo(f"Existing install '{plugin_path}' deleted.")
        else:
            raise click.Abort()

    # install plugin
    src = Path(_HERE, _PLUGIN_NAME)
    if editable:
        plugin_path.symlink_to(src, target_is_directory=True)
    else:
        shutil.copytree(src, plugin_path)

    # generate resource files
    pyqt5ac.main(
        ioPaths=[[f"{plugin_path}/*.qrc", f"{plugin_path}/%%FILENAME%%_rc.py"]]
    )

    click.echo(f"Plugin {_PLUGIN_NAME} installed to '{plugin_path}'")


if __name__ == '__main__':
    install()
