
## Paramètres requis

L'interface graphique du profil propose les paramètres suivants:

- **Fichier d'entrée**: fichier DXF à contrôler
- **SRID des données d'entrée**: projection cartographique des données du fichier DXF
- **Dossier des sorties**: dossier pour que le plugin écrive les différents résultats de son exécution.

### Résultats de l'exécution

- **rapport.xlsx**: rapport d'exécution au format Excel, avec un onglet pour le log de l'exécution et un pour les erreurs/warnings trouvés pour chaque contrôle
- **conversion.gpkg** : conversion au format GeoPackage des données du fichier d'entrée. Ce fichier est utilisé par le plugin pour ses contrôles, et peut être chargé dans QGIS
- **conversion_sig.gpkg** : similaire au fichier ci-dessus, mais avec un seul type géométrique par table et des colonnes différentes selon la table. Ce fichier est prévu pour la lecture dans un SIG ou le chargement dans PostgreSQL
 

## Paramètres optionnels

### Vérifier l'emprise

Ce contrôle permet de vérifier que l'ensemble des géométries du fichier en entrée (hors objets de la famille HABILLAGE) sont strictement incluses dans une emprise.

Ses paramètres sont les suivants:

- **Ficher d'emprise**: Fichier SIG (format GeoJSON, Geopackage, Shapefile) contenant un polygone ou multipolygone correspondant à l'emprise
- **SRID de l'emprise**: projection cartographique de l'emprise


### Vérifier les aberrations planimétriques/altimétriques par rapport à des points de contrôle

Ce contrôle permet de vérifier la précision planimétrique et altimétrique à partir des points de contrôle d'un canevas.

Ce contrôle recherche des points de contrôle aux abords des géométries du fichier en entrée, et compare leur position `(x,y)` ainsi que leur altitude pour détecter
d'éventuelles aberrations.

Les aberrations sont remontées sous la forme de warnings. Les éventuels points non-3D sont remontés sous la forme d'erreurs. 

Ses paramètres sont les suivants:

- **Ficher de points de contrôle**: fichier geopackage comprenant une seule couche de points de canevas (points 3D)
- **SRID des points de contrôle**: projection cartographique des points de contrôle 
- **Ficher CSV de précisions**: il s'agit d'un fichier CSV listant les calques, identifiants d'objets et précisions planimétriques/altimétriques recherchées pour chacun.

Exemple de fichier CSV:
```
"calque","id","prec_plani","prec_alti"
SIGTO_PSI,YP_2900,0.02,0.01
```

### Contrôler les aberrations altimétriques avec un MNT


Ce contrôle permet de vérifier la précision altimétrique à partir d'un Modèle Numérique de Terrain (MNT).

Ce contrôle compare l'altitude du MNT avec celles des géométries du fichier en entrée, afin de détecter d'éventuelles aberrations.

Les aberrations sont remontées sous la forme de warnings. Les éventuels points non-3D sont remontés sous la forme d'erreurs. 


Ses paramètres sont les suivants:

- **Fichier GeoTIFF du MNT**: fichier au format GeoTIFF correspondant à un MNT (valeurs en UINT16, INT16 ou FLOAT32) recouvrant intégralement la zone du projet
- **SRID du MNT**: projection cartographique du MNT
- **Ficher CSV de précisions**: il s'agit d'un fichier CSV listant les calques, identifiants d'objets et précisions altimétriques recherchées pour chacun.

Exemple de fichier CSV:

```
id,calque,prec_alti
YP_1521,SIGTO_SCI,1.0
```
