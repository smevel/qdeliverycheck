import os.path
import configparser
from datetime import datetime


def classFactory(iface):
  from .mainPlugin import Plugin
  return Plugin(iface)


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Define plugin wide constants
PLUGIN_NAME = 'QDeliveryCheck'

# Read metadata.txt
METADATA = configparser.ConfigParser()
METADATA.read(os.path.join(BASE_DIR, 'metadata.txt'), encoding='utf-8')
today = datetime.today()

__version__ = METADATA['general']['version']
__author__ = METADATA['general']['author']
__email__ = METADATA['general']['email']
__web__ = METADATA['general']['homepage']
__help__ = METADATA['general']['help']
__date__ = today.strftime('%Y-%m-%d')
__copyright__ = '(C) {} by {}'.format(today.year, __author__)
