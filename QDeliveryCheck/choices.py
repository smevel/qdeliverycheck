
QUALITE_CATEGORISATION_PCRS_TYPES = (
    ('01', "Très fiable/Conforme PCRS"),
    ('02', "Moyennement fiable"),
    ('03', "Peu fiable"),
)


CATEGORIE_THEMATIQUE_PCRS_TYPES = (
    ('01', "Topo"),
    ('02', "Bâti"),
    ('00', "À déterminer"),
    ('03', "Voirie"),
    ('04', "Ferroviaire"),
    ('05', "Clôture"),
    ('06', "Végétal"),
    ('07', "Ouvrage d'art"),
    ('08', "Hydrographie"),
    ('09', "Orographie"),
    ('10', "Affleurant"),
    ('11', "Raster"),
    ('99', "Non définie"),
)


NATURE_RESEAU_PCRS_TYPES = (
    ('ELEC', "Électricité"),
    ('ELECECL', "Éclairage public"),
    ('ELECTSLT', "Signalisation lumineuse tricolore"),
    ('ELECBT', "Électricité basse tension"),
    ('ELECTHT', "Électricité haute tension"),
    ('GAZ', "Gaz"),
    ('CHIM', "Produits chimiques"),
    ('AEP', "Eau potable"),
    ('ASSA', "Assainissement et pluvial"),
    ('ASSAEP', "Eaux pluviales"),
    ('ASSAEU', "Eaux usées"),
    ('ASSARU', "Réseau unitaire"),
    ('CHAU', "Chauffage et climatisation"),
    ('COM', "Télécom"),
    ('DECH', "Déchets"),
    ('INCEN', "Incendie"),
    ('MULT', "Multi réseaux"),
    ('00', "Non défini"),
)


CATEGORIE_PRECISION_PCRS_TYPES = (
    ('002', "moins de 2 cm"),
    ('005', "de 2 à 5 cm"),
    ('010', "de 5 à 10 cm"),
    ('040', "de 10 à 40 cm"),
    ('150', "de 40 à 150 cm"),
    ('999', "au-delà de 150 cm"),
)
