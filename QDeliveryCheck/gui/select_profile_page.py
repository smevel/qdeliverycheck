from qgis.PyQt.QtWidgets import (
    QComboBox,
    QWizardPage,
    QFormLayout,
    QLabel,
    QVBoxLayout,
    QTextEdit,
)


from QDeliveryCheck.profiles import Profiles

from .forms import forms_registry



class SelectedProfilePage(QWizardPage):

    def __init__(self, wizard):
        self.wizard = wizard
        super().__init__()
        self.setTitle("Profil de validation")

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)

        form_layout = QFormLayout()

        self.select_type = QComboBox()
        for p in Profiles:
            self.select_type.addItem(p.value, p.name)

        self.select_type.currentIndexChanged.connect(self.show_profile_doc)

        form_layout.addRow(QLabel("Type :"), self.select_type)

        main_layout.addLayout(form_layout)

        self.show_doc = QTextEdit()
        self.show_doc.setReadOnly(True)
        main_layout.addWidget(self.show_doc)

        self.init_from_config()

    def init_from_config(self):
        try:
            profile = Profiles[self.wizard.config.plugin['profile']]
        except KeyError:
            pass
        else:
            self.select_type.setCurrentText(profile.value)

    def validatePage(self):
        self.wizard.config.plugin['profile'] = self.select_type.currentData()
        return True

    def initializePage(self):
        self.show_profile_doc()
        profile = self.wizard.config.plugin.get('profile')
        if profile is not None:
            current_index = self.select_type.findData(profile)
            self.select_type.setCurrentIndex(current_index)

    def show_profile_doc(self):
        if self.select_type.currentData():
            current_profile = Profiles[self.select_type.currentData()]
            form_class = forms_registry[current_profile]
            self.show_doc.setHtml(form_class.doc())
        else:
            self.show_doc.setHtml('')


