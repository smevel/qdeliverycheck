from qgis.PyQt.QtCore import QDate
from qgis.PyQt.QtWidgets import (
    QWizardPage,
    QFormLayout,
    QHBoxLayout,
    QLabel,
    QCheckBox,
    QVBoxLayout,
    QLineEdit,
    QComboBox,
    QTableWidget,
    QGroupBox,
    QDateEdit,
)

from QDeliveryCheck.choices import (
    CATEGORIE_PRECISION_PCRS_TYPES,
    CATEGORIE_THEMATIQUE_PCRS_TYPES,
    QUALITE_CATEGORISATION_PCRS_TYPES,
    NATURE_RESEAU_PCRS_TYPES,
)


class GmlComboBox(QComboBox):

    def __init__(self, choices, with_default=False):
        super().__init__()
        if with_default:
            self.addItem("Par défaut", None)
        for value, name in choices:
            self.addItem(name, value)


class BaseGmlTableWidget(QTableWidget):

    categories = NotImplemented

    @property
    def labels(self):
        return [l for _, l in self.categories]

    @property
    def values(self):
        return [v for v, _ in self.categories]

    def make_widget(self):
        raise NotImplementedError

    def get_widget_value(self, widget):
        raise NotImplementedError


    def __init__(self):
        super().__init__()
        self.setShowGrid(False)
        self.setRowCount(len(self.categories))
        self.setVerticalHeaderLabels(self.labels)
        self.setColumnCount(1)
        self.horizontalHeader().hide()
        self.widgets = []
        for i in range(len(self.categories)):
            widget = self.make_widget()
            self.widgets.append(widget)
            self.setCellWidget(i, 0, widget)
        # Stretch the last column to fill widget width
        self.horizontalHeader().setStretchLastSection(True)
        self.resizeColumnsToContents()

    def values_map(self):
        return dict([
            (self.categories[i][0], self.get_widget_value(self.widgets[i]))
            for i in range(len(self.categories))
            if self.get_widget_value(self.widgets[i]) is not None
        ])


class GestionnaireTable(BaseGmlTableWidget):

    categories = NATURE_RESEAU_PCRS_TYPES

    def make_widget(self):
        widget = QLineEdit()
        widget.setPlaceholderText("Par défaut")
        return widget

    def get_widget_value(self, widget):
        value = widget.text().strip()
        if value:
            return value
        return None


class PrecisionTable(BaseGmlTableWidget):

    categories = CATEGORIE_THEMATIQUE_PCRS_TYPES

    def make_widget(self):
        return GmlComboBox(CATEGORIE_PRECISION_PCRS_TYPES, with_default=True)

    def get_widget_value(self, widget):
        return widget.currentData()


class QualiteTable(BaseGmlTableWidget):

    categories = CATEGORIE_THEMATIQUE_PCRS_TYPES

    def make_widget(self):
        return GmlComboBox(QUALITE_CATEGORISATION_PCRS_TYPES, with_default=True)

    def get_widget_value(self, widget):
        return widget.currentData()


def set_combo_value(widget, data):
    if data is None:
        return
    index = widget.findData(data)
    if index != -1:
        widget.setCurrentIndex(index)


class NullableDateWidget(QHBoxLayout):

    date_display_format = 'dd/MM/yyyy'

    serialize_date_format = 'yyyy-MM-dd'

    def __init__(self, label):
        super().__init__()
        self._date = None

        self.date_widget = QDateEdit()
        self.date_widget.setDisplayFormat(self.date_display_format)
        self.date_widget.setCalendarPopup(True)
        self.date_widget.setSpecialValueText(self.inactive_date_display())
        self.date_widget.setDate(self.date_widget.minimumDate())
        self.date_widget.setEnabled(False)
        self.date_widget.dateChanged.connect(self.on_date_changed)

        self.active_widget = QCheckBox(label)
        self.active_widget.stateChanged.connect(self.on_active_change)
        self.active_widget.setChecked(False)

        #self.addRow(self.active_widget, self.date_widget)

        self.addWidget(self.active_widget)
        self.addWidget(self.date_widget, stretch=2)

    def inactive_date_display(self):
        if self._date:
            return self._date.toString(self.date_display_format)
        return ' '

    def is_active(self):
        return self.active_widget.isChecked()

    def on_date_changed(self, date):
        if self.is_active():
            self._date = date

    def on_active_change(self, state):
        if state:
            if not self._date:
                self._date = QDate().currentDate()
            self.date_widget.setDate(self._date)
        else:
            self.date_widget.setSpecialValueText(self.inactive_date_display())
            self.date_widget.setDate(self.date_widget.minimumDate())
        self.date_widget.setEnabled(bool(state))

    def date(self):
        if self.is_active():
            return self._date
        return None

    def text(self):
        if self.date():
            return self.date().toString(self.serialize_date_format)
        return ''

    def from_text(self, date):
        self.active_widget.setChecked(bool(date))
        self.date_widget.setDate(QDate.fromString(date, self.serialize_date_format))



class GMLPage1(QWizardPage):

    base_title = "Configuration de l'export GML"

    def __init__(self, wizard):
        super().__init__()
        self.wizard = wizard

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)

        form_layout = QFormLayout()

        self.do_gml_export = QGroupBox("Exporter en GML")
        self.do_gml_export.setCheckable(True)
        self.do_gml_export.toggled.connect(self.on_form_changed)
        self.do_gml_export.setChecked(True)
        self.do_gml_export.setLayout(QFormLayout())

        self.producer = QLineEdit()
        self.producer.textChanged.connect(self.on_form_changed)
        self.do_gml_export.layout().addRow(QLabel("Nom du producteur :"), self.producer)

        self.prefixe_id = QLineEdit()
        self.do_gml_export.layout().addRow(
            QLabel("Préfixe pour les identifiants :"),
            self.prefixe_id
        )

        gestionnaire_box = QGroupBox("Gestionnaires")
        gestionnaire_box.setLayout(QFormLayout())

        self.gestionnaire = QLineEdit()
        self.gestionnaire.textChanged.connect(self.on_form_changed)
        gestionnaire_box.layout().addRow(QLabel("Par défaut :"), self.gestionnaire)

        self.gestionnaire_table = GestionnaireTable()
        gestionnaire_box.layout().addRow("Par nature de réseau :", self.gestionnaire_table)

        self.do_gml_export.layout().addRow(gestionnaire_box)

        qualite_box = QGroupBox("Qualités")
        qualite_box.setLayout(QFormLayout())

        self.qualite = GmlComboBox(QUALITE_CATEGORISATION_PCRS_TYPES)
        qualite_box.layout().addRow(QLabel("Par défaut :"), self.qualite)

        self.qualite_table = QualiteTable()
        qualite_box.layout().addRow("Par thématique :", self.qualite_table)

        self.do_gml_export.layout().addRow(qualite_box)

        form_layout.addRow(self.do_gml_export)
        main_layout.addLayout(form_layout)

        self.init_from_config()

        self.update_title()

    def update_title(self):
        if self.do_gml_export.isChecked():
            self.setTitle("%s (1/2)" % self.base_title)
        else:
            self.setTitle(self.base_title)

    def init_from_config(self):
        self.do_gml_export.setChecked(
            self.wizard.config.plugin.get('export_gml', True)
        )

        config = self.wizard.config.checker.get('gml_export_options', {})

        self.producer.setText(config.get('producteur', ''))
        self.prefixe_id.setText(config.get('prefixe_id', ''))


        self.gestionnaire.setText(config.get('default_gestionnaire'))
        for index, (reseau, _) in enumerate(NATURE_RESEAU_PCRS_TYPES):
            gestionnaire = config.get('gestionnaire_map', {}).get(reseau)
            if gestionnaire:
                widget = self.gestionnaire_table.cellWidget(index, 0)
                widget.setText(gestionnaire)

        set_combo_value(self.qualite, config.get('default_qualite'))
        for index, (thematique, _) in enumerate(CATEGORIE_THEMATIQUE_PCRS_TYPES):
            qualite = config.get('qualite_map', {}).get(thematique)
            if qualite:
                widget = self.qualite_table.cellWidget(index, 0)
                set_combo_value(widget, qualite)

    def on_form_changed(self):
        self.update_title()
        self.completeChanged.emit()

    def isComplete(self):
        if self.do_gml_export.isChecked():
            if not self.producer.text().strip():
                return False
            if not self.gestionnaire.text().strip():
                return False
        return True

    def validatePage(self):
        self.wizard.config.plugin['export_gml'] = self.do_gml_export.isChecked()
        self.wizard.config.checker.setdefault('gml_export_options', {}).update({
            'producteur': self.producer.text().strip(),
            'prefixe_id': self.prefixe_id.text().strip(),
            'default_gestionnaire': self.gestionnaire.text().strip(),
            'gestionnaire_map': self.gestionnaire_table.values_map(),
            'default_qualite': self.qualite.currentData(),
            'qualite_map': self.qualite_table.values_map(),
        })
        return True

    def nextId(self):
        current = self.wizard.currentId()
        return current + 1 if self.do_gml_export.isChecked() else current + 2



class GMLPage2(QWizardPage):

    def __init__(self, wizard):
        super().__init__()
        self.wizard = wizard
        self.setTitle("Configuration de l'export GML (2/2)")

        main_layout = QVBoxLayout()
        self.setLayout(main_layout)

        form_layout = QFormLayout()

        self.date_leve = NullableDateWidget("Date de levée :")
        form_layout.addRow(self.date_leve)

        self.horodatage = NullableDateWidget("Horodatage :")
        form_layout.addRow(self.horodatage)

        precisionxy_box = QGroupBox("Précision planimétrique")
        precisionxy_box.setLayout(QFormLayout())

        self.precisionxy = GmlComboBox(CATEGORIE_PRECISION_PCRS_TYPES)
        precisionxy_box.layout().addRow(QLabel("Par défaut :"), self.precisionxy)

        self.precisionxy_table = PrecisionTable()
        precisionxy_box.layout().addRow("Par thématique :", self.precisionxy_table)

        form_layout.addRow(precisionxy_box)

        precisionz_box = QGroupBox("Précision altimétrique")
        precisionz_box.setLayout(QFormLayout())

        self.precisionz = GmlComboBox(CATEGORIE_PRECISION_PCRS_TYPES)
        precisionz_box.layout().addRow(QLabel("Par défaut :"), self.precisionz)

        self.precisionz_table = PrecisionTable()
        precisionz_box.layout().addRow("Par thématique :", self.precisionz_table)

        form_layout.addRow(precisionz_box)

        main_layout.addLayout(form_layout)

        if self.wizard.config:
            self.init_from_config()

    def init_from_config(self):
        config = self.wizard.config.checker.get('gml_export_options', {})

        self.date_leve.from_text(config.get('date_leve', None))
        self.horodatage.from_text(config.get('horodatage', None))

        set_combo_value(self.precisionxy, config.get('default_precisionxy'))
        for index, (thematique, _) in enumerate(CATEGORIE_THEMATIQUE_PCRS_TYPES):
            precision = config.get('precisionxy_map', {}).get(thematique)
            if precision:
                widget = self.precisionxy_table.cellWidget(index, 0)
                set_combo_value(widget, precision)

        set_combo_value(self.precisionz, config.get('default_precisionz'))
        for index, (thematique, _) in enumerate(CATEGORIE_THEMATIQUE_PCRS_TYPES):
            precision = config.get('precisionz_map', {}).get(thematique)
            if precision:
                widget = self.precisionz_table.cellWidget(index, 0)
                set_combo_value(widget, precision)

    def on_form_changed(self):
        self.completeChanged.emit()

    def isComplete(self):
        return True

    def validatePage(self):
        self.wizard.config.checker['gml_export_options'].update({
            'date_leve': self.date_leve.text(),
            'horodatage': self.horodatage.text(),
            'default_precisionxy': self.precisionxy.currentData(),
            'precisionxy_map': self.precisionxy_table.values_map(),
            'default_precisionz': self.precisionz.currentData(),
            'precisionz_map': self.precisionz_table.values_map(),
        })
        return True
