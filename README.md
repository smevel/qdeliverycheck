QDeliveryCheck plugin
=====================

QGIS PLugin for 
[DeliveryCheck](https://gitlab.com/someware/deliverycheck/deliverycheck)
python lib.


License
-------

The software is distributed under the terms of the GPL license.

The full license for this software is available in file `LICENCE.txt`.

Develop
-------

Initialize submodules

```
git submodule update --init
```

Install the dev requirements in a virtualenv

```
pip install -r requirements/dev.txt
```

Symlink the plugin into the QGIS plugins root.

```
python install.py -e
```

For a non standard QGIS root or profile name, see options

```
python install.py --help
```

In QGIS, activate the QDeliveryCheck plugin. To be able to reload the code 
without restarting QGIS, use the [Plugin 
Reloader](https://plugins.qgis.org/plugins/plugin_reloader/) plugin.


**Warning** Shapely need to be installed in the QGIS python site package.
Windows users should be fine as shapely is installed by the QGIS installer.
Linux users may have to install it before running the plugin:

```
sudo apt-get install python3-shapely
```


Run tests
---------

To run the tests for the plugin and the DeliveryCheck lib:

```
pytest
```


Run docs
--------

```
mkdocs serve
```

or build a static site from docs

```
mkdocs build
```


Package a release
-----------------

```
qgis-plugin-ci package [release-name]
```

This will generate a ZIP archive installable into QGIS

> Plugins \> Manage and install plugins... \> Install from ZIP


Copyright and license
---------------------

Copyright © 2023 [Someware](https://www.someware.fr)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a [copy of the GNU General Public License](/LICENSE.md)
along with this program. If not, see <https://www.gnu.org/licenses/>.


Initial development was funded by:

-  Région Bretagne
-  Région Pays de Loire
-  Lorient Agglomération
-  Rennes Métropole
-  Brest Métropole
-  Lannion-Trégor Communauté
-  SDEF
-  SDE 22
-  SDE 35
-  Morbihan Énergies
-  Redon Agglomération
-  Quimperlé Communauté
-  Ploërmel Communauté
-  Communauté de Communes du Pays Fouesnantais
